<?
class ECW_CustomerSetGroup_Model_Observer extends Mage_Cron_Model_Observer 
{
	public function update()
	{
		$groupId = 7; //Geschäftskunden 
		$umsatz_limit = 500; //500€ Umsatz im Jahr
		
		$date = new DateTime();
		$year = $date->format('Y');
		
		$orderCollection = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('status',array(
					array('attribute' =>'status', 'eq' => 'complete'),
					array('attribute' =>'status', 'eq' => 'closed')
				)
			)
			->addAttributeToFilter('created_at', array(
				'gt' => $year))
			->addAttributeToFilter('created_at', array(
		    	'lt' => (string)($year+1)))
			->addAttributeToFilter('customer_id',array('notnull' => true));
		
		$customers = array();
		
		foreach($orderCollection as $order) {
			$id = $order->getData('customer_id');
			if(isset($customers[$id])){
				$customers[$id] = $customers[$id] + $order->getData('base_total_paid');
			}else{
				$customers[$id] = $order->getData('base_total_paid');
			}
		}
		
		foreach ($customers as $key => $value) {
			if($value >= $umsatz_limit){
				$customer = Mage::getModel('customer/customer')->load($key);
				$customer->setGroupId($groupId)->save();
				unset($customer);
			}
		}
	}
}